<?php

namespace App\Twig;

use App\Service\MarkdownHelper;
use App\Service\UploadHelper;
use Twig\Extension\RuntimeExtensionInterface;

class AppRuntime implements RuntimeExtensionInterface
{
    private $markdownHelper;

    private $uploadHelper;

    public function __construct(MarkdownHelper $markdownHelper, UploadHelper $uploadHelper)
    {
        $this->markdownHelper = $markdownHelper;
        $this->uploadHelper = $uploadHelper;
    }

    public function processMarkdown($value)
    {
        return $this->markdownHelper->parse($value);
    }

    public function getUploadedAssetPath(string $path): string
    {
        return $this->uploadHelper->getPublicPath($path);
    }
}
