<?php

namespace App\Service;

use App\Helper\LoggerTrait;
use Michelf\MarkdownInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Security\Core\Security;

class MarkdownHelper
{
    use LoggerTrait;

    private $cache;

    private $markdown;

    private $isDebug;

    private $security;

    public function __construct(
        AdapterInterface $cache,
        MarkdownInterface $markdown,
        LoggerInterface $markdownLogger,
        bool $isDebug,
        Security $security
    ) {
        $this->cache = $cache;
        $this->markdown = $markdown;
        $this->logger = $markdownLogger;
        $this->isDebug = $isDebug;
        $this->security = $security;
    }

    public function parse(?string $source): ?string
    {
        if (is_null($source)) {
            return null;
        }

        if (stripos($source, 'bacon')) $this->logMessage(
            LogLevel::INFO,
            'they are talking about bacon again!',
            ['user' => $this->security->getUser()]
        );

        $transformSource = $this->markdown->transform($source);
        if ($this->isDebug) return $transformSource;

        $item = $this->cache->getItem('markdown_' . md5($source));
        if (!$item->isHit()) {
            $item->set($transformSource);
            $this->cache->save($item);
        }

        return $item->get();
    }
}