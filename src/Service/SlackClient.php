<?php

namespace App\Service;

use App\Helper\LoggerTrait;
use Nexy\Slack\Client;
use Psr\Log\LogLevel;

class SlackClient
{
    use LoggerTrait;

    /**
     * @var Client
     */
    private $slack;

    public function __construct(Client $slack)
    {
        $this->slack = $slack;
    }

    /**
     * @param string $message
     * @param string $channel
     */
    public function sendMessage(string $message, string $channel = '#testo-contesto')
    {
        $this->logMessage(LogLevel::INFO, "sent to slack message: {$message}");

        $this->slack->createMessage()
                    ->to($channel)
                    ->from('chaosMONK')
                    ->withIcon(':monkey_face:')
                    ->setText($message)
                    ->send();
    }

}