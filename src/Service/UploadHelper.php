<?php

namespace App\Service;

use Exception;
use Gedmo\Sluggable\Util\Urlizer;
use League\Flysystem\AdapterInterface;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Asset\Context\RequestStackContext;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadHelper
{
    const ARTICLE_IMAGE = 'article_image';

    const ARTICLE_REFERENCE = 'article_reference';

    private $uploadsFilesystem;

    private $requestStackContext;

    private $logger;

    private $publicAssetBaseUrl;

    public function __construct(
        RequestStackContext $requestStackContext,
        FilesystemInterface $uploadsFilesystem,
        LoggerInterface $logger,
        string $uploadedAssetsBaseUrl
    ) {
        $this->uploadsFilesystem = $uploadsFilesystem;
        $this->requestStackContext = $requestStackContext;
        $this->logger = $logger;
        $this->publicAssetBaseUrl = $uploadedAssetsBaseUrl;
    }

    public function getPublicPath(string $path): string
    {
        $fullPath = $this->publicAssetBaseUrl . '/' . $path;

        // if it's already absolute, just return
        if (strpos($fullPath, '://') !== false) {
            return $fullPath;
        }

        // needed if you deploy under a subdirectory
        return $this->requestStackContext
                ->getBasePath() . $fullPath;
    }

    public function uploadArticleImage(File $uploadedFile, ?string $existingFilename): string
    {
        $newFilename = $this->uploadFile($uploadedFile, self::ARTICLE_IMAGE, true);

        if (!$existingFilename) {
            return $newFilename;
        }

        try {
            $result = $this->uploadsFilesystem->delete($existingFilename);
            if ($result === false) {
                throw new Exception("Could not delete existed file {$existingFilename}");
            }
        } catch (FileNotFoundException $e) {
            $this->logger->alert("Old uploaded file {$existingFilename} was missing when trying to delete");
        }

        return $newFilename;
    }

    public function uploadArticleReference(File $reference): string
    {
        return $this->uploadFile($reference, self::ARTICLE_REFERENCE, false);
    }

    /**
     * @return resource
     */
    public function readStream(string $path)
    {
        $resource = $this->uploadsFilesystem->readStream($path);

        if ($resource === false) {
            throw new Exception("Error opening stream for {$path}");
        }

        return $resource;
    }

    public function deleteFile(string $path)
    {
        if ($this->uploadsFilesystem->delete($path) === false) {
            throw new Exception("Error deleting stream for {$path}");
        }
    }

    private function uploadFile(File $file, string $directory, bool $isPublic): string
    {
        if ($file instanceof UploadedFile) {
            $originalFilename = $file->getClientOriginalName();
        } else {
            $originalFilename = $file->getFilename();
        }

        $filename = Urlizer::urlize(pathinfo($originalFilename, PATHINFO_FILENAME))
            . '-' . uniqid() . '.' . $file->guessExtension();

        $stream = fopen($file->getPathname(), 'r');
        $result = $this->uploadsFilesystem->writeStream(
            $directory . DIRECTORY_SEPARATOR . $filename,
            $stream,
            [
                'visibility' => $isPublic ? AdapterInterface::VISIBILITY_PUBLIC : AdapterInterface::VISIBILITY_PRIVATE
            ]
        );

        if ($result === false) {
            throw new Exception("Could not write uploaded file {$filename}");
        }

        if (is_resource($stream)) {
            fclose($stream);
        }

        return $filename;
    }
}
