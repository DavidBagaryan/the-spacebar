<?php

namespace App\Service;

use App\Entity\Article;
use App\Helper\LoggerTrait;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LogLevel;

class HeartCounter
{
    use LoggerTrait;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function incrementHeart(Article $article)
    {
        $prevHeartCount = $article->getHeartCount();
        $article->setHeartCount($prevHeartCount + 1);
        $this->em->flush();

        $this->logMessage(LogLevel::INFO, sprintf(
                'Article #%d is being hearted! old count: %s, new count %s',
                $article->getId(),
                $prevHeartCount,
                $article->getHeartCount()
            )
        );
    }
}