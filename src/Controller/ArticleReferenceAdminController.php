<?php

namespace App\Controller;

use App\Api\ArticleReferenceUploadApiModel;
use App\Entity\Article;
use App\Entity\ArticleReference;
use App\Service\UploadHelper;
use Aws\S3\S3Client;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\File\File as FileObject;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ArticleReferenceAdminController extends BaseController
{
    /**
     * @Route("/admin/article/{id}/references", name="admin_article_list_references", methods={"GET"})
     * @IsGranted("MANAGE", subject="article")
     */
    public function getReferences(Article $article): Response
    {
        return $this->json($article->getArticleReferences(), Response::HTTP_OK, [], ['groups' => 'main']);
    }

    /**
     * @Route("/admin/article/{id}/references", name="admin_article_add_reference", methods={"POST"})
     */
    public function addArticleReference(
        Article $article,
        Request $request,
        UploadHelper $uploadHelper,
        ValidatorInterface $validator,
        EntityManagerInterface $em,
        SerializerInterface $serializer
    ): Response {
        if ($request->headers->get('Content-Type') === 'application/json') {
            /** @var ArticleReferenceUploadApiModel $uploadApiFileModel */
            $uploadApiFileModel = $serializer->deserialize(
                $request->getContent(),
                ArticleReferenceUploadApiModel::class,
                'json'
            );

            $violations = $validator->validate($uploadApiFileModel);
            if ($violations->count() > 0) {
                return $this->json($violations, Response::HTTP_BAD_REQUEST);
            }

            $tmpPath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'sf_upload_' . $uploadApiFileModel->filename . uniqid();
            file_put_contents($tmpPath, $uploadApiFileModel->getDecodedData());
            $referenceFile = new FileObject($tmpPath);
            $originalFilename = $uploadApiFileModel->filename;
        } else {
            /** @var UploadedFile $referenceFile */
            $referenceFile = $request->files->get('reference');
            $originalFilename = $referenceFile->getClientOriginalName();
        }

        $violations = $validator->validate($referenceFile, [
            new File([
                'maxSize'   => '5M',
                'mimeTypes' => [
                    'image/*',
                    'application/pdf',
                    'application/msword',
                    'application/vnd.ms-excel',
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                    'text/plain',
                ],
            ]),
            new NotBlank([
                'message' => 'please select a reference file',
            ]),
        ]);

        if ($violations->count() > 0) {
            return $this->json($violations, Response::HTTP_BAD_REQUEST);
        }

        $filename = $uploadHelper->uploadArticleReference($referenceFile);

        $articleReference = new ArticleReference($article);
        $articleReference->setFilename($filename);
        $articleReference->setOriginalFilename($originalFilename ?? $filename);
        $articleReference->setMimeType($referenceFile->getMimeType() ?? 'application/octet-stream');

        if (is_file($referenceFile->getPathname())) {
            unlink($referenceFile->getPathname());
        }

        $em->persist($articleReference);
        $em->flush();

        return $this->json($articleReference, Response::HTTP_CREATED, [], ['groups' => 'main']);
    }

    /**
     * @Route("/admin/article/references/{id}", name="admin_article_update_reference", methods={"PUT"})
     */
    public function updateArticleReference(
        ArticleReference $reference,
        EntityManagerInterface $em,
        SerializerInterface $serializer,
        Request $request,
        ValidatorInterface $validator
    ): Response {
        $this->denyAccessUnlessGranted('MANAGE', $reference->getArticle());

        $articleReference = $serializer->deserialize(
            $request->getContent(),
            ArticleReference::class,
            'json',
            [
                'object_to_populate' => $reference,
                'groups'             => 'input',
            ]
        );

        $violations = $validator->validate($articleReference);
        if ($violations->count() > 0) {
            return $this->json($violations, Response::HTTP_BAD_REQUEST);
        }

        //$em->persist($articleReference); //opt
        $em->flush();

        return $this->json($articleReference, Response::HTTP_OK, [], ['groups' => 'main']);
    }

    /**
     * @Route("/admin/article/references/{id}", name="admin_article_delete_reference", methods={"DELETE"})
     */
    public function deleteArticleReference(
        ArticleReference $reference,
        UploadHelper $uploadHelper,
        EntityManagerInterface $em
    ): Response {
        $this->denyAccessUnlessGranted('MANAGE', $reference->getArticle());

        $em->remove($reference);
        $em->flush();

        $uploadHelper->deleteFile($reference->getFilePath());

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/admin/article/references/{id}/download", name="admin_article_download_reference", methods={"GET"})
     */
    public function downloadArticleReference(ArticleReference $reference, S3Client $s3Client, string $s3bucket): Response
    {
        $this->denyAccessUnlessGranted('MANAGE', $reference->getArticle());

        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $reference->getOriginalFilename(),
            $reference->getOriginalFilename()
        );

        $command = $s3Client->getCommand('GetObject', [
            'Bucket'                     => $s3bucket,
            'Key'                        => $reference->getFilePath(),
            'ResponseContentType'        => $reference->getMimeType(),
            'ResponseContentDisposition' => $disposition,
        ]);

        $request = $s3Client->createPresignedRequest($command, '+30 minutes');
        return $this->redirect((string)$request->getUri());
    }

    /**
     * @Route("/admin/article/{id}/references/reorder", methods="POST", name="admin_article_reorder_references")
     * @IsGranted("MANAGE", subject="article")
     */
    public function reorderArticleReferences(Article $article, Request $request, EntityManagerInterface $em): Response
    {
        $orderingIds = json_decode($request->getContent(), true);

        if (!$orderingIds) {
            return $this->json(['error' => 'invalid body'], Response::HTTP_BAD_REQUEST);
        }

        $orderingIds = array_flip($orderingIds);

        foreach ($article->getArticleReferences() as $articleReference) {
            $articleReference->setPosition($orderingIds[$articleReference->getId()]);
        }
        $em->flush();

        return $this->json($article->getArticleReferences(), Response::HTTP_OK, [], ['groups' => 'main']);
    }
}
