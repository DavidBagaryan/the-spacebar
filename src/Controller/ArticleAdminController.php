<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleFormType;
use App\Repository\ArticleRepository;
use App\Service\UploadHelper;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleAdminController extends BaseController
{
    /**
     * @Route("/admin/article", name="admin_article_list")
     */
    public function list(ArticleRepository $articleRepo)
    {
        $articles = $articleRepo->findAll();
        return $this->render('article_admin/list.html.twig', ['articles' => $articles]);
    }

    /**
     * @Route("/admin/article/new", name="admin_article_new")
     */
    public function new(EntityManagerInterface $em, Request $request, UploadHelper $uploadHelper): Response
    {
        $form = $this->createForm(ArticleFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('imageFile')->getData();

            /** @var Article $article */
            $article = $form->getData();

            if ($uploadedFile) {
                $newFilename = $uploadHelper->uploadArticleImage($uploadedFile, $article->getImageFilename());
                $article->setImageFilename($newFilename);
            }

            $em->persist($article);
            $em->flush();

            $this->addFlash('success', 'Article Created! Knowledge is power!');
            return $this->redirectToRoute('admin_article_list');
        }

        return $this->render('article_admin/new.html.twig', ['articleForm' => $form->createView()]);
    }

    /**
     * @Route("/admin/article/{id}/edit", name="admin_article_edit")
     */
    public function edit(
        Article $article,
        Request $request,
        EntityManagerInterface $em,
        UploadHelper $uploadHelper
    ): Response {
        $form = $this->createForm(ArticleFormType::class, $article, [
            /** @see ArticleFormType::buildForm() 90line */
            'include_published_at' => true,
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('imageFile')->getData();

            if ($uploadedFile) {
                $newFilename = $uploadHelper->uploadArticleImage($uploadedFile, $article->getImageFilename());
                $article->setImageFilename($newFilename);
            }

            $em->persist($article);
            $em->flush();

            $this->addFlash('success', 'Article Updated!');

            return $this->redirectToRoute('admin_article_edit', ['id' => $article->getId()]);
        }

        return $this->render('article_admin/edit.html.twig', [
            'articleForm' => $form->createView(),
            'article'     => $article
        ]);
    }

    /**
     * @Route("/admin/article/location-select", name="admin_article_location_select")
     * @IsGranted("ROLE_USER")
     */
    public function getSpecificLocationSelect(Request $request): Response
    {
        if (!$this->isGranted("ROLE_ADMIN_ARTICLE") && $this->getUser()->getArticles()->isEmpty()) {
            throw $this->createAccessDeniedException();
        }

        $dummyArt = new Article();
        $dummyArt->setLocation($request->query->get('location'));
        $form = $this->createForm(ArticleFormType::class, $dummyArt);

        if ($form->has('specificLocationName')) {
            return $this->render('article_admin/_specific_location_name.html.twig', [
                'articleForm' => $form->createView(),
            ]);
        }

        return new Response(null, 204);
    }
}
