<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Service\HeartCounter;
use App\Service\SlackClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     * @param ArticleRepository $repository
     * @return Response
     */
    public function homepage(ArticleRepository $repository)
    {
        $articles = $repository->findAllPublished();
        return $this->render('article/homepage.html.twig', ['articles' => $articles]);
    }

    /**
     * @Route("/news/{slug}", name="article_show")
     *
     * @param Article     $article
     * @param SlackClient $slackClient
     * @return Response
     */
    public function show(Article $article, SlackClient $slackClient)
    {
        if ($article->getSlug() === 'duchess-to-play-croquet-then-they-all-moved-off-and-found-in-it-a')
            $slackClient->sendMessage('new tests for all');
        return $this->render('article/show.html.twig', ['article' => $article]);
    }

    /**
     * @Route("/news/{slug}/heart", name="article_toggle_heart", methods={"POST"})
     *
     * @param Article      $article
     * @param HeartCounter $heartCounter
     * @return JsonResponse
     */
    public function toggleArticle(Article $article, HeartCounter $heartCounter)
    {
        $heartCounter->incrementHeart($article);
        return $this->json(['hearts' => $article->getHeartCount()]);
    }
}