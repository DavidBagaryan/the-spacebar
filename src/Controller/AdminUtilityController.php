<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminUtilityController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ADMIN_ARTICLE")
     * @Route("/admin/utility/users", methods="GET", name="admin_utility_users")
     *
     * @param UserRepository $userRepository
     * @return Response
     */
    public function getUserApi(UserRepository $userRepository, Request $request): Response
    {
        $users = $userRepository->findAllMatching($request->query->get('query'), 5);
        return $this->json(compact('users'), 200, [], ['groups' => 'main']);
    }
}