<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * @return Criteria
     */
    public static function createNonDeletedCommentsCriteria(): Criteria
    {
        return Criteria::create()
                       ->andWhere(Criteria::expr()->eq('isDeleted', false))
                       ->orderBy(['createdAt' => 'DESC']);
    }

    /**
     * @param string $orderBy
     * @return Article[]
     */
    public function findAllPublished(string $orderBy = 'publishedAt'): array
    {
        return $this->addIsPublished()
                    ->leftJoin('a.tags', 't')
                    ->addSelect('t')
                    ->addOrderBy("a.$orderBy", 'DESC')
                    ->getQuery()
                    ->getResult();
    }

    /**
     * @param QueryBuilder|null $qb
     * @return QueryBuilder
     */
    private function addIsPublished(QueryBuilder $qb = null): QueryBuilder
    {
        return $this->getOrCreateQB($qb)
                    ->andWhere('a.publishedAt IS NOT NULL');
    }


    /**
     * @param QueryBuilder|null $qb
     * @return QueryBuilder
     */
    private function getOrCreateQB(QueryBuilder $qb = null): QueryBuilder
    {
        return $qb ?: $this->createQueryBuilder('a');
    }

    /**
     * @return Article[]
     */
    public function findAllPublishedLastWeekByAuthor(User $author): array
    {
        return $this
            ->createQueryBuilder('a')
            ->andWhere('a.author = :author')
            ->andWhere('a.publishedAt > :week_ago')
            ->setParameter('author', $author)
            ->setParameter('week_ago', new \DateTime('-1 week'))
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Article[] Returns an array of Article objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
