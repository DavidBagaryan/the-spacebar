<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ArticleStatsCommand extends Command
{
    protected static $defaultName = 'article:stats';

    protected function configure()
    {
        $this->setDescription('Return some article stats')
             ->addArgument('slug', InputArgument::REQUIRED, 'The article slug')
             ->addOption(
                 'format',
                 'f',
                 InputOption::VALUE_OPTIONAL,
                 'The output format',
                 'json'
             );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $slug = $input->getArgument('slug');

        $result = compact(['slug']) + ['hearts' => rand(10, 1000)];

        if ($slug) $io->note("The slug you've passed: {$slug}");

        switch ($format = $input->getOption('format')) {
            case 'text':
                $rows = [];
                foreach ($result as $key => $value) $rows[] = [$key, $value];
                $io->table(['key', 'value'], $rows);
                break;
            case 'json':
                $io->write(\GuzzleHttp\json_encode($result));
                break;
            default:
                throw new \Exception("What kind of crazy format is that? {$format}");
        }

        $io->success('check it out');

        return 0;
    }
}
