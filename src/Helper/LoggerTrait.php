<?php

namespace App\Helper;

use Psr\Log\LoggerInterface;

trait LoggerTrait
{
    /**
     * @var LoggerInterface|null
     */
    private $logger;

    /**
     * @required
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        if (!$this->logger) $this->logger = $logger;
    }

    /**
     * @param mixed  $level
     * @param string $message
     * @param array  $context
     */
    private function logMessage($level, string $message, array $context = [])
    {
        if ($this->logger) $this->logger->log($level, $message, $context);
    }
}