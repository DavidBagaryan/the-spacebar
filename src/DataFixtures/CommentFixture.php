<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CommentFixture extends BaseFixture implements DependentFixtureInterface
{
    public function loadData()
    {
        $this->createMany(98, 'main_comments', function ($i) {
            $comment = new Comment();
            $comment->setAuthorName($this->faker->name);
            $comment->setContent(
                $this->faker->boolean
                    ? $this->faker->paragraph
                    : $this->faker->sentences(10, true)
            );
            $comment->setCreatedAt($this->faker->dateTimeBetween('-30 months', '-12 seconds'));
            $comment->setIsDeleted($this->faker->boolean(20));
            $comment->setArticle($this->getRandomReference('main_articles'));

            return $comment;
        });
        $this->manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return [
            ArticleFixture::class
        ];
    }
}
