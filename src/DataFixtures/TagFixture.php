<?php

namespace App\DataFixtures;

use App\Entity\Tag;

class TagFixture extends BaseFixture
{
    public function loadData()
    {
        $this->createMany(12, 'main_tags', function ($i) {
            $tag = new Tag();
            $tag->setName($this->faker->realText(20));

            return $tag;
        });
        $this->manager->flush();
    }
}
