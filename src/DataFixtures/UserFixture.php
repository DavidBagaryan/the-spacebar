<?php

namespace App\DataFixtures;

use App\Entity\ApiToken;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends BaseFixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function loadData()
    {
        $this->createMany(10, 'main_users', function ($i) {
            $user = new User();
            $user->setEmail("spacebar{$i}@example.com");
            $user->setFirstName($this->faker->firstName);
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user, 'pisUn'
            ));
            $user->agreeTerms();
            if ($this->faker->boolean) {
                $user->setTwitterUsername($this->faker->userName);
            }
            if ($this->faker->boolean(35)) {
                $user->setSubscribeToNewsletter(true);
            }

            $token1 = new ApiToken($user);
            $token2 = new ApiToken($user);

            $this->manager->persist($token1);
            $this->manager->persist($token2);

            return $user;
        });

        $this->createMany(3, 'admin_users', function ($i) {
            $user = new User();
            $user->setEmail("admin{$i}@thespacebar.com");
            $user->setFirstName($this->faker->firstName);
            $user->setRoles(['ROLE_ADMIN']);
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user, 'admin'
            ));
            $user->agreeTerms();
            if ($this->faker->boolean) $user->setTwitterUsername($this->faker->userName);
            return $user;
        });

        $this->manager->flush();
    }
}
