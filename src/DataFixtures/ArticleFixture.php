<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Service\UploadHelper;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

class ArticleFixture extends BaseFixture implements DependentFixtureInterface
{
    private static $images = [
        'asteroid.jpeg',
        'lightspeed.png',
        'mercury.jpeg',
        'meteor-shower.jpg',
    ];

    private $uploadHelper;

    public function __construct(UploadHelper $uploadHelper)
    {
        $this->uploadHelper = $uploadHelper;
    }

    public function loadData()
    {
        $this->createMany(30, 'main_articles', function ($i) {
            $article = new Article();
            $article->setTitle($this->faker->realText(35))
                    ->setContent(<<<EOF
Spicy **jalapeno bacon** ipsum dolor amet veniam shank in dolore. Ham hock nisi landjaeger cow,
lorem proident [beef ribs](https://baconipsum.com/) aute enim veniam ut cillum pork chuck picanha. Dolore reprehenderit
labore minim pork belly spare ribs cupim short loin in. Elit exercitation eiusmod dolore cow
**turkey** shank eu pork belly meatball non cupim.

Laboris beef ribs fatback fugiat eiusmod jowl kielbasa alcatra dolore velit ea ball tip. Pariatur
laboris sunt venison, et laborum dolore minim non meatball. Shankle eu flank aliqua shoulder,
capicola biltong frankfurter boudin cupim officia. Exercitation fugiat consectetur ham. Adipisicing
picanha shank et filet mignon pork belly ut ullamco. Irure velit turducken ground round doner incididunt
occaecat lorem meatball prosciutto quis strip steak.

Meatball adipisicing ribeye bacon strip steak eu. Consectetur ham hock pork hamburger enim strip steak
mollit quis officia meatloaf tri-tip swine. Cow ut reprehenderit, buffalo incididunt in filet mignon
strip steak pork belly aliquip capicola officia. Labore deserunt esse chicken lorem shoulder tail consectetur
cow est ribeye adipisicing. Pig hamburger pork belly enim. Do porchetta minim capicola irure pancetta chuck
fugiat.
EOF
                    );

            if ($this->faker->boolean(72))
                $article->setPublishedAt($this->faker->dateTimeBetween('-10 days', '-1 day'));

            $article->setAuthor($this->getRandomReference('main_users'))
                    ->setHeartCount($this->faker->numberBetween(1, 200))
                    ->setImageFilename($this->getImageFilename());

            $tags = $this->getRandomReferences('main_tags', $this->faker->numberBetween(0, 8));
            foreach ($tags as $tag) $article->addTag($tag);
            return $article;
        });

        $this->manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixture::class,
            TagFixture::class,
        ];
    }

    private function getImageFilename(): string
    {
        $randomImage = $this->faker->randomElement(self::$images);
        $tmpImgDir = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $randomImage;

        $fs = new Filesystem();
        $fixturesImagesPath = __DIR__ . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;
        $fs->copy($fixturesImagesPath . $randomImage, $tmpImgDir);
        //dd(is_file($tmpImgDir));

        return $this->uploadHelper->uploadArticleImage(new File($tmpImgDir), null);
    }
}
