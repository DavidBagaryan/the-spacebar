<?php

namespace App\Form;

use App\Entity\Article;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotNull;

class ArticleFormType extends AbstractType
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * ArticleFormType constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Article|null $article */
        $article = $options['data'] ?? null;
        $isEdit = $article && $article->getId();

        $builder
            ->add('title', TextType::class, [
                'help' => 'stranger in the night?',
            ])
            ->add('content', null, [
                'rows' => 18,
            ])
            ->add('author', UserSelectEmailType::class, [
                'disabled' => $isEdit,
                'attr'     => ['class' => 'foo'],
            ])
            ->add('location', ChoiceType::class, [
                'placeholder' => 'Choose a location',
                'choices'     => [
                    'The Solar System'   => 'solar_system',
                    'Near a star'        => 'star',
                    'Interstellar Space' => 'interstellar_space',
                ],
                'required'    => false,
            ])
            ->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) {
                    /** @var Article|null $data */
                    $data = $event->getData();
                    if (!$data) {
                        return;
                    }
                    $this->setupSpecificLocationNameField($event->getForm(), $data->getLocation());
                }
            );

        $imageConstraints = [
            new Image(['maxSize' => '5M']),
        ];

        if (!$isEdit || !$article->getImageFilename()) {
            $imageConstraints[] = new NotNull([
                'message' => 'Please upload an image',
            ]);
        }

        $builder->add('imageFile', FileType::class, [
            'mapped'      => false,
            'required'    => false,
            'constraints' => $imageConstraints,
        ]);

        if ($options['include_published_at']) {
            $builder->add('publishedAt', DateTimeType::class, [
                'placeholder' => [
                    'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
                ],

                //'format' => 'dd-MM-yyyy H:i',
                //'widget' => 'single_text',
            ]);
        }

        $builder->get('location')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $formEvent) {
                $form = $formEvent->getForm();
                $this->setupSpecificLocationNameField($form->getParent(), $form->getData());
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'           => Article::class,
            'include_published_at' => false,
        ]);
    }

    private function getLocationNameChoices(string $location)
    {
        $planets = [
            'Mercury',
            'Venus',
            'Earth',
            'Mars',
            'Jupiter',
            'Saturn',
            'Uranus',
            'Neptune',
        ];
        $stars = [
            'Polaris',
            'Sirius',
            'Alpha Centauari A',
            'Alpha Centauari B',
            'Betelgeuse',
            'Rigel',
            'Other',
        ];
        $locationNameChoices = [
            'solar_system'       => array_combine($planets, $planets),
            'star'               => array_combine($stars, $stars),
            'interstellar_space' => null,
        ];

        return $locationNameChoices[$location] ?? null;
    }

    private function setupSpecificLocationNameField(FormInterface $form, ?string $location)
    {
        if (is_null($location)) {
            $form->remove('specificLocationName');
            return;
        }

        $choices = $this->getLocationNameChoices($location);
        if (is_null($choices)) {
            $form->remove('specificLocationName');
            return;
        }

        $form->add('specificLocationName', ChoiceType::class, [
            'placeholder' => 'Where exactly?',
            'choices'     => $choices,
            'required'    => false,
        ]);

    }
}